package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource(value = "classpath:/properties/redis.properties",
                encoding = "utf-8")
public class RedisController2 {
    @Value("${redis.host2}")
    private String host; //="168.1.100.111";

    @Value("${redis.port2}")
    private int port;    //=7000;

    @RequestMapping("/getNode2")
    public  String getNode2(){
        return host+"  |  "+port;
    }
}
