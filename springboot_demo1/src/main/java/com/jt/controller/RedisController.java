package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {
    @Value("${redis.host}")
    private String host; //="168.1.100.111";

    @Value("${redis.port}")
    private int port;    //=7000;

    @RequestMapping("/getNode1")
    public  String getNode1(){
        return host+"  |  "+port;
    }
}
