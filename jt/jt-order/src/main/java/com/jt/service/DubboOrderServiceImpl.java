package com.jt.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.OrderItemMapper;
import com.jt.mapper.OrderMapper;
import com.jt.mapper.OrderShippingMapper;
import com.jt.pojo.Order;
import com.jt.pojo.OrderItem;
import com.jt.pojo.OrderShipping;

@Service
public class DubboOrderServiceImpl implements DubboOrderService {
	
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private OrderShippingMapper orderShippingMapper;
	@Autowired
	private OrderItemMapper orderItemMapper;


	@Override
	@Transactional
	public String saveOrder(Order order) {
		String orderId=""+order.getUserId()+ System.currentTimeMillis();
		//1完成订单入库
		order.setOrderId(orderId).setStatus(1);
		orderMapper.insert(order);
		System.out.println("订单入库成功");

		//2.完成订单物流信息
		OrderShipping orderShipping=order.getOrderShipping();
		orderShipping.setOrderId(orderId);
		orderShippingMapper.insert(orderShipping);
		System.out.println("订单物流信息入库");
		//3.订单商品信息入库
		List<OrderItem> orderItems=order.getOrderItems();
		for(OrderItem orderItem:orderItems){
			orderItem.setItemId(orderId);
			orderItemMapper.insert(orderItem);
			System.out.println("订单商品入库");
		}
		return orderId;
	}

	@Override
	public Order findOrderById(String id) {
		//1.根据orderId查询查询order对象
		Order order=orderMapper.selectById(id);
		//2.根据orderId查询查询orderShipping对象
		OrderShipping orderShipping=orderShippingMapper.selectById(id);
		//3.根据orderId查询查询orderItem信息
		QueryWrapper<OrderItem> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("order_id",id);
		List<OrderItem> itemList=orderItemMapper.selectList(queryWrapper);
		return order;
	}
}
