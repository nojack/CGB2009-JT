package com.jt.utill;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtil {
    private  static final ObjectMapper MAPPER=new ObjectMapper();
    //1 object->json
    public static  String toJson(Object obj){
       try {
          return MAPPER.writeValueAsString(obj);
       }catch (JsonProcessingException e){
           //
           e.printStackTrace();
           throw new RuntimeException(e);
       }

    }

    //2 json->object
    public  static  <T> T toObj(String json ,Class<T> target){
       try {
          return MAPPER.readValue( json, target);
       }catch (JsonProcessingException e){
           e.printStackTrace();
           throw new RuntimeException(e);
       }
    }

}
