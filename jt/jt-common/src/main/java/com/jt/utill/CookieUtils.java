package com.jt.utill;

import org.springframework.boot.web.servlet.server.Session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {
    /**
     * 1.save cookie
     */
    public  static  void addCookie(HttpServletResponse response,String cookieName, String cookieValue, String domain, int seconds){
        Cookie cookie=new Cookie(cookieName,cookieValue);
        cookie.setDomain(domain);
        cookie.setMaxAge(seconds);
        cookie.setPath("/");
        response.addCookie(cookie);
    }
    /**
     * 2.get cookie
     */
    public static Cookie getCookie(HttpServletRequest request,String cookieName){
        Cookie cookieTemp=null;
        Cookie[] cookies=request.getCookies();
        if(cookies!=null&&cookies.length>0){
            for (Cookie cookie:cookies){
                if(cookieName.equals(cookie.getName())){
                    cookieTemp = cookie;
                    break;
                }
            }
        }
        return cookieTemp;
    }
    /**
     * 3.get cookie的值
     */
    public static String getCookieValue(HttpServletRequest request,String cookieName){
        Cookie cookie=getCookie(request,cookieName);
        return cookie==null?null:cookie.getName();
    }
}
