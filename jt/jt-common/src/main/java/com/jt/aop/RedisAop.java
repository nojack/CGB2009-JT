package com.jt.aop;

import com.jt.annotation.CacheFind;
import com.jt.utill.ObjectMapperUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.ShardedJedis;

import java.util.Arrays;

@Component//交给Spring容器管理
@Aspect//标识我是一个切面
public class RedisAop {
    @Autowired
     //private Jedis jedis;       //redis 单台测试
     //private ShardedJedis jedis;  //redis 分片测试
     //private JedisSentinelPool sentinelPool;
    private JedisCluster jedis;
    /**
     * 注意事项:  当有多个参数时,joinPoint必须位于第一位.
     * 需求:
     * 1.准备key= 注解的前缀 + 用户的参数
     * 2.从redis中获取数据
     * 有: 从缓存中获取数据之后,直接返回值
     * 没有: 查询数据库之后再次保存到缓存中即可.
     * <p>
     * 方法:
     * 动态获取注解的类型,看上去是注解的名称,但是实质是注解的类型. 只要切入点表达式满足条件
     * 则会传递注解对象类型.
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
  // @Around("@annotation(com.jt.annotation.CacheFind)")
  //  public Object around(ProceedingJoinPoint joinPoint,
  //                                        ) throws Throwable {
    @Around("@annotation(cacheFind)")
    public Object around(ProceedingJoinPoint joinPoint,
                         CacheFind cacheFind) throws Throwable {

        Object result = null;
        String preKey = cacheFind.Key();
        String key = preKey + "::" + Arrays.toString(joinPoint.getArgs());

        //从池中, 动态获取数据
        //Jedis jedis=sentinelPool.getResource();
        //1.校验redis中是否有数据
        if (jedis.exists(key)) {
              //如果数据存在,需要从redis中获取json数据,之后直接返回
            String json = jedis.get(key);
            //1.获取方法对象,   2.获取方法的返回值类型
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            Class returnType = methodSignature.getReturnType();
            result = ObjectMapperUtil.toObj(json, returnType);
            System.out.println("AOP查询redis缓存");
        } else {
            result = joinPoint.proceed();
            //将数据转化为JSON
            String json = ObjectMapperUtil.toJson(result);
            if (cacheFind.seconds() > 0) {
                jedis.setex(key, cacheFind.seconds(), json);
            } else{
                jedis.set(key, json);
            }
            System.out.println("AOP查询数据库");
        }
        //jedis.close();
        return result;













        //Aop入门
        //切面 = 切入点表达式 + 通知方法
        //表达式1: bean(itemCatServiceImpl)  ItemCatServiceImpl类
        //@Pointcut("bean(itemCatServiceImpl)")
        //@Pointcut("within(com.jt.service.*)")
        // .* 一级包下的类   ..* 所有子孙后代的包和类
        //返回值类型任意, com.jt.service包下的所有类的add方法参数类型任意类型
        //写参数类型时注意类型的大小写
        //@Pointcut("execution(* com.jt.service..*.*(..))")
        /**
         public void pointcut(){

         }

         /**
         * joinPoint代表连接点对象,一般适用于前四大通知类型(除around之外的)
         *        客人                         路人
         */
    /*
    @Before("pointcut()")
    public void before(JoinPoint  joinPoint){
        //1.获取目标对象
        Object target=joinPoint.getTarget().getClass();
        System.out.println(target);
        //2.获取目标对象的路径 包名.类名.方法名
        String className=joinPoint.getSignature().getDeclaringTypeName();
        String methodName=joinPoint.getSignature().getName();
        System.out.println("目标对象的路径"+(className+"."+methodName));
        //3.获取参数类型
        System.out.println(Arrays.toString(joinPoint.getArgs()));
    }
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint)  {
        System.out.println("环绕通知执行");
        Object data=null;
       try {
           data=joinPoint.proceed();
       }catch (Throwable throwable){
           throwable.printStackTrace();
       }
       return data;
    }
    */
    }
}