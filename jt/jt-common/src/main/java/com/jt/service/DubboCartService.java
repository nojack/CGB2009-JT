package com.jt.service;

import com.jt.pojo.Cart;
import com.jt.pojo.Order;

import java.util.List;

public interface DubboCartService {
    List<Cart> findCartListByUserId(long userId);

    void updateNum(Cart cart);

    void addCart(Cart cart);

    void deleteCart(Cart cart);


}
