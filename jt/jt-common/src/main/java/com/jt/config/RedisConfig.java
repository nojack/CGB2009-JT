package com.jt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.*;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@PropertySource("classpath:/redis.properties")
@Configuration//配置类 一般与@bean连用
public class RedisConfig {

    @Value("${redis.nodes}")
    private String nodes;

    @Bean
    public JedisCluster jedisCluster(){
        Set<HostAndPort> nodeSet=new HashSet<>();
        String[] nodeArray=nodes.split(",");
        for(String node : nodeArray){
            String host=node.split(":")[0];
            int port=Integer.parseInt(node.split(":")[1]);
            HostAndPort hostAndPort=new HostAndPort(host,port);
            nodeSet.add(hostAndPort);
        }
        return  new JedisCluster(nodeSet);
    }



   /* @Value("${redis.sentinel}")
    private String sentinel;

    @Bean
    public JedisSentinelPool jedisSentinelPool(){
        //1.设定连接池大小
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMinIdle(5);
        jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMaxTotal(100);
        //2.链接哨兵的集合
        Set<String> sentinels=new HashSet<>();
        sentinels.add(sentinel);
        return new JedisSentinelPool("mymaster",sentinels,jedisPoolConfig);
    }*/


    /*@Value("${redis.nodes}")
    private String nodes;

    @Bean
    public ShardedJedis shardedJedis(){
        List<JedisShardInfo> shards=new ArrayList<>();
            String[] nodeArray=nodes.split(",");
            for(String node :nodeArray){
                String host=node.split(":")[0];
                int port=Integer.parseInt(node.split(":")[1]);
                JedisShardInfo jedisShardInfo=new JedisShardInfo(host,port);
                shards.add(jedisShardInfo);
            }
        return new ShardedJedis(shards);
    }*/


    /*@Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private Integer port;*/

    /*@Bean
    public Jedis jedis(){
        return new Jedis(host,port);
    }*/


}
