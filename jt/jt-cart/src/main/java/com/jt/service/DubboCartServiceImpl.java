package com.jt.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.CartMapper;
import com.jt.pojo.Cart;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


@Service(timeout = 3000)
public class DubboCartServiceImpl  implements DubboCartService{

    @Autowired
    private CartMapper cartMapper;
    @Override
    public List<Cart> findCartListByUserId(long userId) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("user_id",userId);
        return cartMapper.selectList(queryWrapper);
    }

    /**
     * sql update tb_cart set num=#{num} ,updated=#{updated} where
     * item_id=#{itemId} and user_id=#{userId}
     *param cart
     * upadte operate:  entity表示更新数据
     */
    @Override
    public void updateNum(Cart cart) {
        Cart cartTemp=new Cart();
        cartTemp.setNum(cart.getNum());
        QueryWrapper<Cart> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("user_id", cart.getUserId());
        queryWrapper.eq("item_id",cart.getItemId());
        cartMapper.update(cartTemp,queryWrapper);
    }
    /**
     *业务逻辑
     * 如果购物车已存在数据则更新数量
     * 如果没有数据则新政数据
     *
     * update sql：
     *    update tb_cart set num=#{num} where id=#{id}
     *  param: cart
     */

    @Override
    public void addCart(Cart cart) {
        //1.查询数据库 校验是否有数据，user_id item_id
        QueryWrapper<Cart> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("item_id",cart.getItemId());
        queryWrapper.eq("user_id",cart.getUserId());
        Cart cartDB=cartMapper.selectOne(queryWrapper);
        if(cartDB==null){
            cartMapper.insert(cart);
        }else{
            //更新数据库数量
            int num=cart.getNum()+cartDB.getNum();
            Cart cartTemp=new Cart();
            cartTemp.setId(cartDB.getId());
            cartTemp.setNum(num);
            cartMapper.updateById(cartTemp);
        }
    }

    @Override
    public void deleteCart(Cart cart) {
        cartMapper.delete(new QueryWrapper<>(cart));
    }
}
