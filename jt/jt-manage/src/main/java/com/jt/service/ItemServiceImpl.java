package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.ItemMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;

	/**
	 * 分页Sql:
	 * 查询第一页:
	 * select * from tb_item limit 起始位置,查询条数
	 * select * from tb_item limit 0,20    共21个数  index[0,19]
	 * 查询第二页:
	 * select * from tb_item limit 20,20    index[20,39]
	 * 查询第三页:
	 * select * from tb_item limit 40,20
	 * 查询第N页:
	 * * 	 	select * from tb_item limit (page-1)rows,20
	 *
	 * @param page
	 * @param rows
	 * @return
	 */
	@Override
	public EasyUITable findItemByPage(int page, int rows) {
		//查询所有记录
		/*List<Item> itemList =itemMapper.selectList(null);
		long total=itemMapper.selectCount(null);
		return new EasyUITable(total,itemList);*/

		//1.手写分页
		/*long total=itemMapper.selectCount(null);
		int startIndex = (page - 1) * rows;
		List<Item> itemList = itemMapper.findItemByPage(startIndex, rows);
		return  new EasyUITable(total,itemList);*/

		//2.利用MP方式实现分页
		IPage iPage=new Page(page,rows);
		QueryWrapper <Item>queryWrapper=new QueryWrapper();
		queryWrapper.orderByDesc("updated");
		itemMapper.selectPage(iPage,queryWrapper);
		long total=iPage.getTotal();//获取记录总数
		List<Item> itemList=iPage.getRecords();
		return new EasyUITable(total,itemList);
	}


	@Override
	@Transactional
	public void saveItem(Item item, ItemDesc itemDesc) {
		item.setStatus(1)
			.setCreated(new Date())
			.setUpdated(new Date());
		itemMapper.insert(item);
		//商品详情入库 mp自动回显
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);
	}

	@Override
	@Transactional
	public void updateItem(Item item,ItemDesc itemDesc) {
		itemMapper.updateById(item);
		itemDesc.setItemId(item.getId());
		itemDescMapper.updateById(itemDesc);
	}

	@Override
	public void deleteItem(Long[] ids) {
		//1 mp方式进行删除操作
		/*List<Long> longIds= Arrays.asList(ids);
		itemMapper.deleteBatchIds(longIds);*/
		//2 书写sql方式实现数据删除
		itemMapper.deleteItem(ids);
		//删除商品详情信息
		itemDescMapper.deleteBatchIds(Arrays.asList(ids));
	}

	@Override
	public void updateStatus(Integer[] ids, Integer status) {
		//1.以MP的方式操作数据库  只修改状态码/updated时间
		/*Item item=new Item();
		item.setStatus(status);
		UpdateWrapper updateWrapper=new UpdateWrapper();
		updateWrapper.in("id",Arrays.asList(ids));
		itemMapper.update(item,updateWrapper );*/
		//2.手写SQL;
		itemMapper.updateStatus(ids,status);
	}

	@Override
	public ItemDesc findItemDescById(Long itemId) {
		return itemDescMapper.selectById(itemId);
	}


}
