package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.annotation.CacheFind;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.utill.ObjectMapperUtil;
import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService{
    @Autowired
    private ItemCatMapper itemCatMapper;
    @Autowired(required = false)
    private Jedis jedis;
    @Override
    @CacheFind(Key ="ITEM_CAT_ID")
    public ItemCat findItemCatName(Long itemCatId) {

        return itemCatMapper.selectById(itemCatId);
    }
    /**
     * 1.根据parentId 查询数据库记录
     * 2.将数据库的List集合转化为VO对象的集合.
     *
     * @param parentId
     * @return
     */
    @Override
    @CacheFind(Key="ITEM_CAT_PARENTID,seconds=7*24*60*60")
    public List<EasyUITree> findItemCatLIst(Long parentId) {
        List<EasyUITree> treeList=new ArrayList<>();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("parent_id",parentId);
        List<ItemCat> itemCatList=itemCatMapper.selectList(queryWrapper);

        for(ItemCat itemCat:itemCatList){
            long id=itemCat.getId();
            String text=itemCat.getName();
            String state=itemCat.getIsParent()?"closed":"open";
                                //是父级先关闭用户需要时打开
            EasyUITree easyUITree=new EasyUITree(id,text,state);
            treeList.add(easyUITree);
        }
        return treeList;
    }


    /**
     *  缓存处理业务逻辑:
     *      *     1.第一次查询先查缓存
     *      *     2.结果:
     *      *           有结果:    直接从缓存中获取
     *      *           没有结果:  第一次查询,先查询数据库,之后保存到缓存中
     *      *     3.Redis.set(0,json)
     *      *     @param parentId
     *      * @return
     * 实现步骤:
     *      1.先定义key     ITEM_CAT_PARENT::0
     *      2.先查询缓存
     *          有  true    获取缓存数据之后,将JSON转化为对象 之后返回
     *          没有 false   应该查询数据库, 之后将数据保存到redis中. 默认30天超时.
     * * @param parentId
     * @return
     */

    @Override
    public List<EasyUITree> findItemCatCache(Long parentId) {
        List<EasyUITree> treeList =new ArrayList<>();
        Long startTime=System.currentTimeMillis();
        //1.定义key
        String key="ITEM_CAT_PARENTID::"+parentId;
        //2.从缓存中获取对象
        if(jedis.exists(key)){
            //存在  直接获取缓存数据,之后转化为对象
           String json=jedis.get(key);
           treeList=ObjectMapperUtil.toObj(json,treeList.getClass());
           Long endTime=System.currentTimeMillis();
           System.out.println("查询缓存时间"+(endTime-startTime)+"毫秒");

        }else{
            treeList=findItemCatLIst(parentId);
            String json=ObjectMapperUtil.toJson(treeList);
            jedis.set(key,json);
            Long endTime=System.currentTimeMillis();
            System.out.println("查询数据库时间"+(endTime-startTime)+"毫秒");

        }
        return treeList;
    }


}
