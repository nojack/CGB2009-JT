package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
@PropertySource("classpath:/properties/image.properties")
public class FileServiceImpl implements FileService {

    private static Set<String> sets = new HashSet<>();
    @Value("${image.localDir}")
    private String localDir;  //="D:/JT-SOFT/images"; //定义磁盘文件根目录
    @Value("${image.urlPath}")
    private String urlPath;   //="http://image.jt.com";  //设定域名地址

    static {
        sets.add(".jpg");
        sets.add(".png");
        sets.add(".gif");
        //....
    }

    /**
     * 1.校验图片的类型    a.jpg
     * 2.校验是否为恶意程序
     * 3.采用分目录方式进行存储
     * 4.防止文件重名 动态生成ID
     *
     * @param uploadFile
     * @return
     */
    @Override
    public ImageVO upload(MultipartFile uploadFile) {
        //1.获取图片名称   a.jpg | A.JPG
        String fileName = uploadFile.getOriginalFilename();
        fileName = fileName.toLowerCase();
        //2.校验是否为图片 .jpg
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        if (!sets.contains(fileType)) {
            //如果不属于类型,则表示不是图片
            return ImageVO.fail();
        }

        //3.校验是否为恶意程序
        try {
            BufferedImage bufferedImage =
                    ImageIO.read(uploadFile.getInputStream());
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();

            if (width == 0 || height == 0) {
                return ImageVO.fail();
            }
            //4.采用分目录方式进行存储
            String dateDir = new SimpleDateFormat("/yyyy/MM/dd").format(new Date());
            //D:/JT-SOFT/images/2020/12/1/
            //5.定义磁盘文件存储目录D:/JT-SOFT/images/yyyy/MM/dd
            String fileDirPath = localDir + dateDir;
            File dirFile = new File(fileDirPath);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            String uuid= UUID.randomUUID().toString();
            String realFileName=uuid+fileType;

            uploadFile.transferTo(new File(fileDirPath+realFileName));

            String url=urlPath+dateDir+realFileName;
            return ImageVO.success(url,width,height);
        } catch (IOException e) {
            e.printStackTrace();
            return ImageVO.fail();
        }

    }
}


