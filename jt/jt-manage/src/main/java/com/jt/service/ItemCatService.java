package com.jt.service;

import com.jt.pojo.ItemCat;
import com.jt.vo.EasyUITree;

import java.util.List;

public interface ItemCatService {
    ItemCat findItemCatName(Long itemCatId);

    List<EasyUITree> findItemCatLIst(Long parentId);

    List<EasyUITree> findItemCatCache(Long parentId);
}
