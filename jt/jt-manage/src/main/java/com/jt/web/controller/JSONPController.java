package com.jt.web.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;
import com.jt.utill.ObjectMapperUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JSONPController {
    /**
     * url  http://manage.jt.com/web/testJSONP?callback=jQuery11110902477319930389_1610351407265&_=1610351407266
     * param  callback
     * return callback(json)
     */
    /*@RequestMapping("/web/testJSONP")
    public String jsonp(String callback) {
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("desc");
        String json = ObjectMapperUtil.toJson(itemDesc);
        return callback + "(" + json + ")";
    }*/

    //@RequestMapping("/web/testJSONP")
    public JSONPObject jsonp2(String callback) {
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("desc");
        return new JSONPObject(callback, itemDesc);
    }

    @RequestMapping("/web/testJSONP")
   // @CrossOrigin(value = "http:www.jt.com",methods = {RequestMethod.GET,RequestMethod.POST})
    @CrossOrigin  //所有请求都允许跨域访问
    public ItemDesc itemDesc() {
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("AAA");
        return itemDesc;
    }
}
