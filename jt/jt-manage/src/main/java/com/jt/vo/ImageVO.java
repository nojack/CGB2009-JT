package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ImageVO {
    private Integer error;// 0 表示文件上传正确    1.上传有误
    private String url;
    private Integer width;
    private Integer height;
    public  static  ImageVO fail(){
        return new ImageVO(1,null,null,null);
    }
    public  static ImageVO success(String url,Integer width,Integer height){
        return  new ImageVO(0,url,width,height);
    }
}