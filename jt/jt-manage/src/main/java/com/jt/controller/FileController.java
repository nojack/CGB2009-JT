package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
public class FileController {
    @Autowired
    private FileService fileService;
    /**
     * url地址:  http://localhost:8091/file
     * 参数:     文件信息 fileImage
     * 返回值:   String
     */
    @RequestMapping("/file")
    public String file(MultipartFile fileImage) throws IOException {
        //1.获取图片名称
        String name=fileImage.getOriginalFilename();
        //2.定义文件目录
        String fileDirPath="D:/JT-SOFT/images";
        //3.创建目录
        File fileDir=new File(fileDirPath);
        if(!fileDir.exists()){
            fileDir.mkdirs();
        }
        //4.生成文件的全路径
        String filePath=fileDirPath+"/"+name;
        File imageFile=new File(filePath);
        //5.实现文件上传
        fileImage.transferTo(imageFile);
        return "文件上传成功";
    }
    /**
     *
     * 业务说明: 文件上传
     * url:  //localhost:8091/pic/upload?dir=image
     * 参数:  // 编辑器参数
     * 	kingEditorParams : {
     * 		filePostName  : "uploadFile",
     * 		uploadJson : '/pic/upload',
     * 		dir : "image"
     * 返回值: ImageVO
     */
    @RequestMapping("/pic/upload")
    public ImageVO Upload(MultipartFile uploadFile){

        return fileService.upload(uploadFile);
    }
}
