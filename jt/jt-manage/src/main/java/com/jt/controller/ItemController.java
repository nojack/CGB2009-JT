package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.FileService;
import com.jt.vo.EasyUITable;
import com.jt.vo.ImageVO;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jt.service.ItemService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;


	/**
	 * 1.实现数据展现
	 * URL：http://localhost:8091/item/query?page=1&rows=20
	 * param: page=1 页数  &rows=20  行数
	 * 返回值: EasyUITable
	 */
	@RequestMapping("/query")
	public EasyUITable findItemByPage(int page,int rows){

		return itemService.findItemByPage(page,rows);
	}


	/**
	 * 1.实现商品新增
	 * URL：http://localhost:8091/item/save
	 * param:form表单数据
	 * 返回值: 	SysResult
	 */
	@RequestMapping("/save")
	public SysResult saveItem(Item item, ItemDesc itemDesc){
			itemService.saveItem(item,itemDesc);
			return SysResult.success();
	}

	/**
	 * 1.实现商品修改
	 * URL：http://localhost:8091/item/update
	 * param:form表单数据提交
	 * 返回值: 	SysResult
	 */
	@RequestMapping("/update")
	public SysResult updateItem(Item item,ItemDesc itemDesc){
		itemService.updateItem(item,itemDesc);
		return SysResult.success();
	}
	/**
	 * 1.实现商品删除
	 * URL：http://localhost:8091/item/delete
	 * param: ids=100，101，102
	 * 返回值: 	SysResult
	 */
	@RequestMapping("/delete")
	public SysResult deleteItem(Long[] ids){
		itemService.deleteItem(ids);
		return SysResult.success();
	}
	/**
	 * 实现商品上架/下架操作
	 * url:/item/updateStatus/2
	 * 参数: 1/2代表商品的状态, ids=100,101,102,103
	 * 返回值: SysResult对象
	 * SpringMVC框架: 参数接收说明 如果参数中间以逗号的形式分割
	 * 则可以自动的转化为数组类型
	 */
	@RequestMapping("/{status}")
	public SysResult updateStatus(@PathVariable Integer status , Integer[] ids){
		itemService.updateStatus(ids,status);
		return SysResult.success();
	}

	/**
	 * 业务说明: 根据商品Id号,检索商品详情信息
	 * url:  http://localhost:8091/item/query/item/desc/1474392023
	 * 参数:  result风格获取数据.
	 * 返回值: SysResult对象
	 */
	@RequestMapping("/query/item/desc/{itemId}")
	public SysResult findItemDescById(@PathVariable Long itemId){
		ItemDesc itemDesc=itemService.findItemDescById(itemId);
		return SysResult.success(itemDesc);
	}
}
