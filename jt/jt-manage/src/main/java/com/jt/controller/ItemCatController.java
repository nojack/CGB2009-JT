package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.service.ItemCatServiceImpl;
import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;
    /**
     * 需求: 根据itemCatId查询商品分类名称
     * url:  http://localhost:8091/item/cat/queryItemName?itemCatId=163
     * 参数: itemCatId=163
     * 返回值: 商品分类名称
     */
    @RequestMapping("/item/cat/queryItemName")
    public String findItemCatName(Long itemCatId){
        ItemCat itemCat=itemCatService.findItemCatName(itemCatId);
        return itemCat.getName();
    }

    /**
     * 树形结构展现
     * url: item/cat/list
     * params: parent_id=0
     * return： easyUITree
     */
    @RequestMapping("/item/cat/list")
    public List<EasyUITree> findItemCatList(Long id){
        Long parentId=(id == null)?0:id;
        //重数据库获取
        return itemCatService.findItemCatLIst(parentId);
        //重缓存获取
        //return itemCatService.findItemCatCache(parentId);
    }


}
