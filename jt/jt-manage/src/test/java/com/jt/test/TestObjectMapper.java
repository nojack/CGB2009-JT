package com.jt.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.ItemDesc;
import org.junit.jupiter.api.Test;

public class TestObjectMapper {

    /**
     *
     */
    @Test
    public void test01() throws JsonProcessingException {
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100l).setItemDesc("object转化json测试");
        ObjectMapper objectMapper = new ObjectMapper();
        //1 object->json
        String json = objectMapper.writeValueAsString(itemDesc);
        System.out.println(json);
        //2 json->object
        ItemDesc itemDesc2=objectMapper.readValue(json, ItemDesc.class);
        System.out.println(itemDesc2);
    }


}
