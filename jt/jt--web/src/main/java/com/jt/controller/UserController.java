package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.service.HttpClientService;
import com.jt.utill.CookieUtils;
import com.jt.vo.SysResult;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private HttpClientService httpClientService;
    @Reference
    private DubboUserService dubboUserService;
    @Autowired
    private JedisCluster jedisCluster;
    /**
     *http://www.jt.com/user/login.html      login.jsp
     * http://www.jt.com/user/regisetr.html  register.jsp
     */

    @RequestMapping("/{moduleName}")
    public  String moduleName(@PathVariable String moduleName){
        return  moduleName;
    }

    /**
     * 完成HttpClient测试
     * 1.url地址: http://www.jt.com/user/findUserList
     * 2.请求参数: 无
     * 3.返回值结果: List<User>集合
     */
    @RequestMapping("/findUserList")
    @ResponseBody
    public List<User> findUserList(){

        return httpClientService.findUserList();
    }

    /**
     * 完成用户注册    公司开发时:业务接口文档
     *  url地址:  http://www.jt.com/user/doRegister
     *  参数:     注册表单提交
     *  业务逻辑:  接收用户信息,将数据密码之后保存到数据库中
     *  返回值:   SysResult对象
     */
    @RequestMapping("/doRegister")
    @ResponseBody   //转化JSON
    public SysResult doRegister(User user){

        dubboUserService.doRegister(user);
        return SysResult.success();
    }

    /**
     * url: http://www.jt.com/user/doLogin?r=0.4511522931461409
     * 参数: username/password
     * 返回值: SysResult对象
     *
     * cookie相同知识:
     *  http://xxx/addUser;
     *  http://xxx/aaa/addUser;
     *
     *  1.cookie.setPath("/aaa");
     */
    @RequestMapping("/doLogin")
    @ResponseBody
    public SysResult doLogin(User user, HttpServletResponse response){
        //1.判断数据是否有效
        if(StringUtils.isEmpty(user.getUsername())||StringUtils.isEmpty(user.getPassword())){
            return  SysResult.fail();
        }
        //2。完成用户信息校验
        String ticket=dubboUserService.findUserByUP(user);
        //3.判断返回值是否有效
        if(StringUtils.hasLength(ticket)){
            Cookie cookie = new Cookie("JT_TICKET",ticket);
            cookie.setMaxAge(30*24*60*60);  //30天超时
            cookie.setPath("/");            //设定cookie访问路径
            cookie.setDomain("jt.com");     //设定数据共享
            response.addCookie(cookie);     //上传Cookie
            return SysResult.success();
        }
        return SysResult.fail();

    }
    /**
     * 完成用户退出操作
     * 1.重定向到系统首页
     * 2.要求删除redis中的数据  K-V结构  先获取key
     * 3.动态获取Cookie中的数据
     * 4.删除Cookie中的数据
     * url地址: http://www.jt.com/user/logout.html
     *
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request,HttpServletResponse response){
       String ticket= CookieUtils.getCookieValue(request,"JT_TICKET");
       if(StringUtils.hasLength(ticket)){
           jedisCluster.del(ticket);//删除redis中的数据
           //删除cookie必须与原始Cookie保持一致
           CookieUtils.addCookie(response,"JT_TICKET","","jt.com",0 );
       }
        //跳转系统首页
        return "redirect:/";
    }
}

