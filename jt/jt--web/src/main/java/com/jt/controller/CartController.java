package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.util.UserThreadLocal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Reference(check = false)
    private DubboCartService dubboCartService;
    /**
     * url: http://www.jt.com/cart/show.html
     * 返回值: 购物车列表页面 cart.jsp
     * 页面数据取值:  ${cartList}
     * request对象取值：只能在controller层中
     */
    @RequestMapping("/show")
    public String cartShow(Model model, HttpServletRequest request) {
        //1.获取用户信息 ticket  redis  userJSON user对象 userId
        User user = (User) request.getAttribute("JT_USER");
        long userId=user.getId();
        List<Cart> cartList=dubboCartService.findCartListByUserId(userId);
        model.addAttribute("cartList",cartList);
        return "cart";
    }

    /**
     * 实现商品数量的更新操作
     * url地址:   http://www.jt.com/cart/update/num/562379/8
     * 参数:      itemId/num
     * 返回值:    void
     */
    @RequestMapping("/update/num/{itemId}/{num}")
    @ResponseBody
    public void updateNum(Cart cart){
        long userId= UserThreadLocal.getUser().getId();
        cart.setUserId(userId);
        dubboCartService.updateNum(cart);
    }

    /**
     * 实现购物车新政入库操作
     * URL：http://www.jt.com/cart/add/562379.html
     * param：  form表单
     *return：重定向到购物车列表页面
     */
    @RequestMapping("/add/{itemId}")
    public String addCart(Cart cart){
        long userId= UserThreadLocal.getUser().getId();
        cart.setUserId(userId);
        dubboCartService.addCart(cart);
        return "redirect:/cart/show.html";
    }

    /**实现购物车删除操作
     * url：http://www.jt.com/cart/delete/1474391973.html
     *param： 562379 userId /itemId
     * return: 重定向到购物车列表页面http://www.jt.com/cart/show.html
     */
    @RequestMapping("/delete/{itemId}")
    public String deleteCart(Cart cart){
        long userId= UserThreadLocal.getUser().getId();
        cart.setUserId(userId);
        dubboCartService.deleteCart(cart);
        return "redirect:/cart/show.html";
    }

}
