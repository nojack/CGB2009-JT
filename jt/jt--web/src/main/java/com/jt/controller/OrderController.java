package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.Order;
import com.jt.service.DubboCartService;
import com.jt.service.DubboOrderService;
import com.jt.util.UserThreadLocal;
import com.jt.vo.SysResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Reference(check = false)
    private DubboCartService cartService;
    @Reference(check = false)
    private DubboOrderService orderService;
    /**
     *url:http://www.jt.com/order/create.html
     * param :userId
     * retirn:order-cart.jsp
     * 页面取值方式${carts}
     */
    @RequestMapping("/create")
    public String orderCart(Model model){
        long userId= UserThreadLocal.getUser().getId();
        List<Cart> cartList=cartService.findCartListByUserId(userId);
        model.addAttribute("carts", cartList);
        return "order-cart";
    }

    /**
     * 提交订单
     * url:http://www.jt.com/order/submit
     * param:整个order表单
     * return：sysResult对象
     *        返回orderId
     */
    @RequestMapping("/submit")
    @ResponseBody
    public SysResult saveOrder(Order order){
        long userId=UserThreadLocal.getUser().getId();
        order.setUserId(userId);
        String orderId=orderService.saveOrder(order);
        return  SysResult.success(orderId);
    }

    /**
     * 查看订单
     * url：http://www.jt.com/order/success.html?id=71610706854413
     * param ：id=71610706854413
     * return: success.jsp
     * 页面取值：${order.orderId}
     */
    @RequestMapping("/success")
    public String findOrder(String id,Model model){
        Order order=orderService.findOrderById(id);
        model.addAttribute("order",order);
        return "success";
    }
}
