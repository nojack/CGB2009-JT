package com.jt.util;

import com.jt.pojo.User;

public class UserThreadLocal {
    private static ThreadLocal<User> threadLocal=new ThreadLocal<>();
    //存数据
    public static void setUser(User user){
        threadLocal.set(user);
    }
    //取数据
    public static User getUser(){
        return threadLocal.get();
    }

    public static void remove(){
        threadLocal.remove();
    }
}
