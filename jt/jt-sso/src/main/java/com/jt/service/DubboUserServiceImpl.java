package com.jt.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.utill.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import redis.clients.jedis.JedisCluster;

import java.util.UUID;

@Service
public class DubboUserServiceImpl implements DubboUserService{

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JedisCluster jedisCluster;

    /**
     * 注意事项:
     *      1.暂时使用电话号码 代替email
     *      2.需要对密码进行加密处理   md5Hash加密
     * @param user
     */
    @Override
    public void doRegister(User user) {
        user.setEmail(user.getPhone());
        String md5Pass =              //实现方法加密
                DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Pass);
        userMapper.insert(user);
    }

    @Override
    public String findUserByUP(User user) {
        //1.密码加密
        String md5Password=DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Password);
        //2.根据username password 查询数据，基于对象不为空属性当做where条件
        QueryWrapper<User> queryWrapper=new QueryWrapper<>(user);
        User userDB=userMapper.selectOne(queryWrapper);
        //3.判断用户信息是否有
        if(userDB==null){
            //如果查询结果为null,则表示用户名或密码错误
            return null;
        }
        //4..如果用户名和密码正确则开始单点登录操作
        String ticket = UUID.randomUUID().toString().replace("-", "");
        //5.需要将用户数据转户为JSON  需要将数据进行脱敏处理
        userDB.setPassword("密码是123456你信不");
        String json= ObjectMapperUtil.toJson(userDB);
        //6.将数据保存到redis中 30天自动失效
        int seconds=30*24*60*60;
        jedisCluster.setex(ticket,seconds,json);
        return ticket;
    }
}