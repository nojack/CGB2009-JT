package com.jt.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.User;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.service.UserService;
import redis.clients.jedis.JedisCluster;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private JedisCluster jedisCluster;
	@RequestMapping("/findUserAll")
	public List<User> findAll(){
		return userService.findAll();
	}


	/**
	 *  JSONP
	 * 实现用户数据校验
	 * url:http://sso.jt.com/user/check/{param}/{type}
	 * 参数: /{param} 用户需要校验的数据
	 *      /{type}   校验的字段.
	 * 返回值: SysResult对象(true/false)
	 */
	@RequestMapping("/check/{param}/{type}")
	public JSONPObject checkUser(@PathVariable String param,
								 @PathVariable Integer type,
								 String callback){
		//查询数据库获取响应信息.
		boolean flag=userService.checkUser(param ,type);
		SysResult sysResult=SysResult.success(flag);
		//int a=1/0; //出现异常时解决
		return  new JSONPObject(callback ,sysResult);
		//callback(JSON结构)
	}

	/**
	 * 完成HttpClient业务调用
	 * url地址: http://sso.jt.com/user/findUserList
	 * 返回值: UserJSON
	 */
	@RequestMapping("/findUserList")
	public List<User> findUserList(){
		return userService.findUserList();
	}

	/**jsonp跨域完成登录用户信息回显
	 * URL: http://sso.jt.com/user/query/192e9445-d9ea-4514-a585-c61a102f0a56?callback=jsonp1610596990449&_=1610596990617
	 * param ticket信息
	 * return callback (sysresult对象)
	 */
	@RequestMapping("/query/{ticket}")
	public JSONPObject findUserByTicket(@PathVariable String ticket,
										String callback){
		String json=jedisCluster.get(ticket);
		if(StringUtils.hasLength(json)){
			return new JSONPObject(callback, SysResult.success(json));

		}else{
			return new JSONPObject(callback,SysResult.fail());
		}
	}
}
