package com.jt.controller;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/findAll")
    public String findAll(Model model){
    List<User> userList=userMapper.selectList(null);
    model.addAttribute("userList",userList);
        return "userList";
    }
    /**
     * ajax说明:
     *       1.跳转到含有ajax页面
     *       2.浏览器解析ajax 发起ajax请求实现业务功能
     */

    //1.跳转到ajax页面
    @RequestMapping("ajax")
    public  String ajax(){
    return "userAjax";
    }

    //2.接收Ajax请求  /userAjax
    @RequestMapping("/userAjax")
    @ResponseBody
    public List<User>  userAjax(){
        return userMapper.selectList(null);
    }

}
