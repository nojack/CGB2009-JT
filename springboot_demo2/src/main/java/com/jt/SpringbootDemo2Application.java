package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication//项目启动类
public class SpringbootDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemo2Application.class, args);
	}

}
