package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class User  {
    @TableId(type= IdType.AUTO)
    private Integer id;
    //@TableField(value = "name")
    private String name;
    //@TableField(value = "age")
    private Integer age;
    //@TableField(value = "sex")
    private String sex;

}
