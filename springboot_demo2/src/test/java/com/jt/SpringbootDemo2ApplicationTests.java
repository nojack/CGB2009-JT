package com.jt;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class SpringbootDemo2ApplicationTests {
	@Autowired
	private UserMapper userMapper;

	/**
	 * 将name="测试案例" 修改为 "测试环境"
	 * 参数说明: entity 需要改为的数据对象
	 * 			 updateWrapper 更新条件构造器
	 * 特点: 将对象中不为null的属性当做set条件
	 * */
	@Test
	public void testUpdate(){
		User user=new User();
		user.setName("测试环境");
		UpdateWrapper<User> updateWrapper=new UpdateWrapper<>();
		updateWrapper.eq("name","测试案例");
		userMapper.update(user,updateWrapper);
	}
	/**
	 *包含“君”并且(或者)sex为女性，按照id降序排列
	 */
	@Test
	public  void testSelect05(){
	QueryWrapper <User>queryWrapper=new QueryWrapper();
	queryWrapper.like("name","君")
				.or()
				.eq("sex","女")
				.orderByDesc("id");
		System.out.println(userMapper.selectList(queryWrapper));
	}
	/**
	 * 数据按照age降序排列，如果age相同，按照Id降序排列
	 */
	@Test
	public void testSelect04(){
	QueryWrapper queryWrapper=new QueryWrapper();
	queryWrapper.orderByDesc("age","id");
		System.out.println(userMapper.selectList(queryWrapper));
	}

	/**
	 * 查询"精"
	 * sql： select  .... where name like '%精%'
	 */
	@Test
	public void testSelect03(){
		QueryWrapper queryWrapper=new QueryWrapper();
		queryWrapper.like("name","精");
		System.out.println(
				userMapper.selectList(queryWrapper));

		//2.模糊查询2
		QueryWrapper queryWrapper2=new QueryWrapper();
		queryWrapper2.likeRight("name","孙");
		System.out.println(
				userMapper.selectList(queryWrapper2));
	}

	/**
	 * 查询id=1，3，5，6数据
	 */
	@Test
	public void testSelect02(){
		Integer[] ids={1,3,5,6};
		//将数组转化list集合
		List<Integer> idList= Arrays.asList(ids);
		QueryWrapper queryWrapper=new QueryWrapper();
		queryWrapper.in("id",idList);
		List<User> userList=userMapper.selectList(queryWrapper);
		System.out.println(userList);
		//批量查询
		userMapper.selectBatchIds(idList);
	}



	@Test
	public void testSelect(){
	User user=userMapper.selectById(5);
		System.out.println(user);
		List<User> userList=userMapper.selectList(null);
		System.out.println(userList);
		//查询name=“唐僧”
		//常见关系运算符 =eq ,> gt , < lt ,>=ge,<=le
		QueryWrapper queryWrapper=new QueryWrapper();
		queryWrapper.eq("name","唐僧");
		List<User> userList1= userMapper.selectList(queryWrapper);
		System.out.println(userList1);
	}

	@Test
	public void testInsert(){
		User user=new User();
		user.setName("鲁班七号").setAge(6).setSex("女");
		userMapper.insert(user);
	}
	@Test
	public void test01(){
		List<User> list=userMapper.findAll();
		System.out.println(list);
	}
	@Test
	void contextLoads() {
		User user=new User();
		user.setId(10).setName("haha").setSex("男");
	}

}
